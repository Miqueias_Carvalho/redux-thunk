import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { addCommentThunk } from "./store/modules/user/thunks";
import { useState } from "react";

function App() {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [comment, setComment] = useState("");

  const handleClick = () => {
    dispatch(addCommentThunk(comment));
    setComment("");
  };

  return (
    <div className="App">
      <header className="App-header">
        <p className="name">{user.name}</p>
        <input
          placeholder="Comentário..."
          value={comment}
          onChange={(e) => setComment(e.target.value)}
        />
        <button onClick={handleClick}>new comment</button>
        <div className="comments">
          {user.comments.map((elem, index) => (
            <p key={index}>{elem}</p>
          ))}
        </div>
      </header>
    </div>
  );
}

export default App;
