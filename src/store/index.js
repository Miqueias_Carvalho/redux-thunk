// adicionamos o applyMiddleweare do próprio redux
import { createStore, combineReducers, applyMiddleware } from "redux";

// também adicionamos o thunk
import thunk from "redux-thunk";

import useReducer from "./modules/user/reducer";

const reducers = combineReducers({ user: useReducer });

// além do reducers, colocamos o applyMiddleware passando o thunk
const store = createStore(reducers, applyMiddleware(thunk));

export default store;
