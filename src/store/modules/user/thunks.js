import { addComment } from "./actions";

//o comment é o que recebemos de fora no caso será o comentário
export const addCommentThunk = (comment) => {
  //no thunk retornamos uma funcao anonima
  return (dispatch, getState) => {
    //aqui estamos pegando o state user
    const { user } = getState();

    // aqui adicionamos o comentário que entrou como parâmetro lá em cima
    const updateUser = { ...user, comments: [...user.comments, comment] };

    // nessa linha damos o dispatch na nossa action, com a alteração feita
    dispatch(addComment(updateUser));
  };
};
